FROM haskell

COPY . .

RUN apt-get update && apt-get install -y libgmp-dev \ 
    netbase libstdc++6 ca-certificates

RUN cabal update

RUN cabal build --only-dependencies -j4

RUN cabal install

ENV PATH /root/.cabal/bin:$PATH
ENV PATH /.cabal/bin:$PATH

EXPOSE 3000


CMD ["websockets-test-exe"]

